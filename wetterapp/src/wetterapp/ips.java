package wetterapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

public class ips {

	public String getprivateIP() throws UnknownHostException {

		String pIp = InetAddress.getLocalHost().getHostAddress();
		return pIp;
			
	}
	
	public String getpublicIP() throws IOException {
			URL ipAdress = new URL("http://myexternalip.com/raw");
        	BufferedReader in = new BufferedReader(new InputStreamReader(ipAdress.openStream()));
        	String ip = in.readLine();

        	return ip;
	}
	
	public String getLocation(String s) throws IOException {
		
		URL ipAdress = new URL("https://tools.keycdn.com/geo.json?city=" + s);
		BufferedReader in = new BufferedReader(new InputStreamReader(ipAdress.openStream()));
		String locale = in.readLine();
		
		return locale;
		
	}		
}
