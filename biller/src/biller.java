import java.util.Scanner;

public class biller 
{

	public static void main(String[] args) 
	{
		do {
			//Deklarieren der Variablen
			Scanner eingabe = new Scanner(System.in);

			float ProdPreis = 0f;
			float Preis = 0f;
			char versandart = ' ';
			boolean stammkunde = false;
			boolean error = false;

			final float freiV = 100;
			final float rabatt = 0.03f;
			final int nVersand = 7;
			final int eVersand = 17;



			//Produktpreis bestimmen
			System.out.print("Geben Sie den Produkpreis ein: ");
			do
			{
				try
				{
					ProdPreis = eingabe.nextFloat();
					error = false;
				}
				catch(Exception e)
				{
					eingabe.next();
					System.out.print("Ung�ltige Eingabe\nBitte geben Sie einen g�ltigen Preis an: ");
					error = true;
				}


			}while(error);
			//System.out.println("Das Produkt Kostet: " + ProdPreis);			//Test



			//Kundenstatus bestimmen
			System.out.print("Sind Sie ein Stammkunde ? (j/n) ");
			do 
			{
				char tmp = Character.toUpperCase(eingabe.next().charAt(0));

				if (tmp == 'J') 
				{
					stammkunde = true;
					error = false;
				}
				else if(tmp == 'N')
				{
					stammkunde = false;
					error = false;
				}
				else
				{
					System.out.print("Ung�ltige Eingabe\nSind Sie ein Stammkunde ? (j/n) ");
					error = true;
				}

			}while (error);
			/*
		if (stammkunde)
		{
			System.out.println("Sie sind Stammkunde");
		}
		else
		{
			System.out.println("Sie sind kein Stammkunde");
		}
			 */



			//Versandart bestimmen
			System.out.print("W�hlen Sie Eine Versandart: [A]bholen / standart [V]ersand / [E]xpress Versand : ");
			do 
			{
				char tmp = Character.toUpperCase(eingabe.next().charAt(0));

				if (tmp == 'A' || tmp == 'V' || tmp == 'E')
				{
					versandart = tmp;
					error = false;
				}
				else
				{
					System.out.print("Ung�ltige Eingabe\n[A]bholen / standart [V]ersand / [E]xpress Versand : ");
					error = true;
				}

			}while(error);
			//System.out.println("Ihre Auswahl: " + versandart);

			switch(versandart)
			{
			case 'A':
				if(stammkunde)
				{
					Preis = ProdPreis - (ProdPreis*rabatt);
				}
				else
				{
					Preis = ProdPreis;
				}
				break;

			case 'V':
				if(ProdPreis > freiV)
				{
					if(stammkunde)
					{
						Preis = ProdPreis - (ProdPreis*rabatt);
					}
					else
					{
						Preis = ProdPreis;
					}	
				}
				else
				{
					if(stammkunde)
					{
						Preis = ProdPreis - (ProdPreis*rabatt) + nVersand;
					}
					else
					{
						Preis = ProdPreis + nVersand;
					}
				}
				break;

			case 'E':
				if(ProdPreis > freiV)
				{
					if(stammkunde)
					{
						Preis = ProdPreis - (ProdPreis*rabatt);
					}
					else
					{
						Preis = ProdPreis;
					}	
				}
				else
				{
					if(stammkunde)
					{
						Preis = ProdPreis - (ProdPreis*rabatt) + eVersand;
					}
					else
					{
						Preis = ProdPreis + eVersand;
					}
				}
				break;
			}
			System.out.println("Preis: " + Preis);
			//try

		}while(true);
	}

}
